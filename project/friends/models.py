from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

class Message(models.Model):

    user_that_sent = models.ForeignKey(
        User, 
        on_delete=models.CASCADE, 
        related_name="userThatSent"
    )
    user_that_received = models.ForeignKey(
        User, 
        on_delete=models.CASCADE, 
        related_name="userThatReceived"
    )
    date_time_of_msg = models.DateTimeField(default=now())
    msg_text = models.TextField()

    def __str__(self):
        return self.msg_text[0:50]

    class Meta:
        db_table = 'message'

class RoleForContact(models.Model):

    '''
    Friend or follower.
    '''

    role_title = models.CharField(max_length=100)

    def __str__(self):
        return self.role_title

    class Meta:
        db_table = 'role_for_contact'

class Contact(models.Model):

    my_id = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        related_name="myId",
        db_index=True
    )
    user_id = models.ForeignKey(
        User, 
        on_delete=models.CASCADE, 
        related_name="userId",
        unique=True,
        db_index=True
    )
    role_id = models.ForeignKey(
        RoleForContact, 
        on_delete=models.CASCADE,
        related_name="roleId",
        db_index=True
    )
    date_contact_from = models.DateTimeField(default=now())
    message_id = models.ForeignKey(
        Message, 
        on_delete=models.SET_NULL, 
        related_name='messageId',
        blank=True, 
        null=True
    )

    def __str__(self):
        return self.user_id.username

    class Meta:
        db_table = 'contact'
        unique_together = (('my_id', 'user_id'),)
