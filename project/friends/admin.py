from django.contrib import admin
from .models import Message, Contact, RoleForContact

admin.site.register(Message)
admin.site.register(Contact)
admin.site.register(RoleForContact)