CREATE TABLE "user"(
	user_id SERIAL PRIMARY KEY NOT NULL,
	first_name CHARACTER VARYING(150),
	last_name CHARACTER VARYING(150),
	email CHARACTER VARYING(200) NOT NULL,
	password CHARACTER VARYING(20) NOT NULL,
	username CHARACTER VARYING(150) NOT NULL UNIQUE
);

CREATE TABLE message(
	message_id SERIAL PRIMARY KEY NOT NULL,
	user_that_sent_id SERIAL REFERENCES "user" NOT NULL,
	user_that_received_id SERIAL REFERENCES "user" NOT NULL,
	date_time_of_msg TIMESTAMPTZ DEFAULT (now() at time zone 'utc')  NOT NULL,
	msg_text TEXT NOT NULL
);

CREATE TABLE role_for_contact(
	role_id SERIAL PRIMARY KEY NOT NULL,
	role_title CHARACTER VARYING(100) NOT NULL
);

CREATE TABLE contact(
	contact_id SERIAL PRIMARY KEY NOT NULL,
	my_id SERIAL REFERENCES "user" NOT NULL,
	user_id SERIAL REFERENCES "user" NOT NULL UNIQUE,
	role_id SERIAL REFERENCES role_for_contact NOT NULL,
	date_contact_from TIMESTAMPTZ DEFAULT (now() at time zone 'utc')  NOT NULL,
	message_id SERIAL REFERENCES message,
	UNIQUE(my_id, user_id)
);

CREATE INDEX role ON contact (role_id);