-- List of all users that's Sergey friends  --
SELECT "user".user_id, "user".username FROM 
	contact INNER JOIN "user" ON contact.user_id = "user".user_id AND contact.my_id = 1 
		WHERE contact.role_id = 1;

-- List of all users that's friends for user that contains list above) --
SELECT "user".user_id, "user".username FROM 
	contact INNER JOIN "user" ON contact.user_id = "user".user_id AND contact.my_id IN (
		SELECT "user".user_id FROM 
		contact INNER JOIN "user" ON contact.user_id = "user".user_id 
		AND contact.my_id = 1
	) WHERE contact.role_id = 1;