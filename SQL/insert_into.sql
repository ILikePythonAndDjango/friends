-- roles --
INSERT INTO role_for_contact VALUES(1, 'Friend');
INSERT INTO role_for_contact VALUES(2, 'Follower');

-- users --
INSERT INTO "user" VALUES(1, null,    null,        'sergey@gmail.com', 'qwer1234', 'Sergey');
INSERT INTO "user" VALUES(2, null,    null,        'anton@gmail.com',  'qwer1234', 'Anton');
INSERT INTO "user" VALUES(3, 'Pavel', 'Petrovich', 'pasha@gmail.com',  'qwer1234', 'Pasha');

-- messages --
INSERT INTO message VALUES(1, 3, 1, TIMESTAMP '19-07-2018 16:32:00', 'Hello! Do you want append my to your friends list?');
INSERT INTO message VALUES(2, 2, 1, TIMESTAMP '19-07-2018 16:33:00', 'I want to be your friend');

-- contacts --
-- Reject request from Pasha to Sergey. Then Pasha is Sergey's follower! --
INSERT INTO contact VALUES(1, 1, 3, 2, TIMESTAMP '19-07-2018 16:43:00', 1);
-- Accept request from Anton to Sergey. Then Anton is Sergey's friend! --
INSERT INTO contact VALUES(2, 1, 2, 1, TIMESTAMP '19-07-2018 16:53:00', 2);

-- Creating user for sampling --
INSERT INTO "user" VALUES
	(4,  null, null, 'aodkes@gmail.com', 'qwer1234', 'Jack'),
	(5,  null, null, 'fcike,@gmail.com', 'qwer1234', 'Sasha'),
	(6,  null, null, 'adnlkd@gmail.com', 'qwer1234', 'John'),
	(7,  null, null, 'dninek@gmail.com', 'qwer1234', 'Jessica'),
	(8,  null, null, 'ivnkmw@gmail.com', 'qwer1234', 'Snoop Dog'),
	(9,  null, null, 'skjdoe@gmail.com', 'qwer1234', 'Lindemman'),
	(10, null, null, 'ssdfsa@gmail.com', 'qwer1234', 'Till'),
	(11, null, null, 'sadsfs@gmail.com', 'qwer1234', 'Paul'),
	(12, null, null, 'ssdfaa@gmail.com', 'qwer1234', 'Kate'),
	(13, null, null, 'sadsde@gmail.com', 'qwer1234', 'LOL'),
	(14, null, null, 'sopjpd@gmail.com', 'qwer1234', 'KEK');

-- Creating message for sampling --
INSERT INTO message VALUES
	(3,  4,  1, TIMESTAMP '19-07-2018 19:33:00', 'I want to be your friend'),
	(4,  5,  2, TIMESTAMP '19-07-2018 19:34:00', 'I want to be your friend'),
	(5,  6,  2, TIMESTAMP '19-07-2018 19:35:00', 'I want to be your friend'),
	(6,  7,  2, TIMESTAMP '19-07-2018 19:36:00', 'I want to be your friend'),
	(7,  8,  2, TIMESTAMP '19-07-2018 19:37:00', 'I want to be your friend'),
	(8,  9,  2, TIMESTAMP '19-07-2018 19:38:00', 'I want to be your friend'),
	(9,  10, 4, TIMESTAMP '19-07-2018 19:39:00', 'I want to be your friend'),
	(10, 11, 4, TIMESTAMP '19-07-2018 19:40:00', 'I want to be your friend'),
	(11, 12, 4, TIMESTAMP '19-07-2018 19:41:00', 'I want to be your friend'),
	(12, 13, 4, TIMESTAMP '19-07-2018 19:42:00', 'I want to be your friend'),
	(13, 14, 4, TIMESTAMP '19-07-2018 19:43:00', 'I want to be your friend');

-- Creating frineds for sampling --
INSERT INTO contact VALUES
	(3,  1, 4,  1, TIMESTAMP '19-07-2018 19:33:10', 3),
	(4,  2, 5,  1, TIMESTAMP '19-07-2018 19:34:10', 4),
	(5,  2, 6,  1, TIMESTAMP '19-07-2018 19:35:10', 5),
	(6,  2, 7,  1, TIMESTAMP '19-07-2018 19:36:10', 6),
	(7,  2, 8,  1, TIMESTAMP '19-07-2018 19:37:10', 7),
	(8,  2, 9,  1, TIMESTAMP '19-07-2018 19:38:10', 8),
	(9,  4, 10, 1, TIMESTAMP '19-07-2018 19:39:10', 9),
	(10, 4, 11, 1, TIMESTAMP '19-07-2018 19:40:10', 10),
	(11, 4, 12, 1, TIMESTAMP '19-07-2018 19:41:10', 11),
	(12, 4, 13, 1, TIMESTAMP '19-07-2018 19:42:10', 12),
	(13, 4, 14, 1, TIMESTAMP '19-07-2018 19:43:10', 13);
